import tkinter as tk
import sortIntoFoldersOfX

window = tk.Tk()

title = tk.Label(text="Sort Into Folders Of X")
title.pack()

sourcePathLabel = tk.Label(
    text="Input the source folder path (D:\path\\to\\folder)",
    # fg="white",
    # bg="black",
    # width=10,
    # height=2
)
sourcePathLabel.pack()
sourcePathInput = tk.Entry(fg="black", bg="white", width=50)
sourcePathInput.pack()
sourcePathErrorLabel = tk.Label(
    text="Input the source folder path (D:\path\\to\\folder)",
    fg="red",
    # bg="black",
    # width=10,
    # height=2
)
sourcePathLabel.pack()

numberOfFilesPerFolderLabel = tk.Label(
    text="Input number of files per folder",
    # fg="white",
    # bg="black",
    # width=10,
    # height=2
)
numberOfFilesPerFolderLabel.pack()
numberOfFilesPerFolderInput = tk.Entry(fg="black", bg="white", width=50)
numberOfFilesPerFolderInput.pack()

button = tk.Button(
    text="Generate & Sort",
    # width=25,
    # height=5,
    # bg="blue",
    # fg="yellow",
)
button.pack()

# entry.delete(0, tk.END)

# sortIntoFoldersOfX.sourceOfFiles=sourcePathInput
# sortIntoFoldersOfX.numberOfFilesPerFolder=numberOfFilesPerFolderInput

window.mainloop()