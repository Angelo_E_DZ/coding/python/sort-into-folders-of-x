import os.path
from os import path
from pyparsing import Or
# import tkinter as tk
# import gui

isAnInvalidDirectory = True
fileCounter = 0
folderNumber = 1
fileAndFolderList = []
directoryExists = False

# get and validate the user input for the source directory
while isAnInvalidDirectory:
    sourceOfFiles = str(input("Input the source folder path (D:\path\\to\\folder): "))
    if (not os.path.exists(sourceOfFiles)):
        print('Invalid path, Please enter a valid path')
        isAnInvalidDirectory = True
    else:
        isAnInvalidDirectory = False

# get and validate the user input for the number of files per folder
while True:
    try:
        numberOfFilesPerFolder = int(input("Input number of files per folder: "))
    except ValueError:
        print("Invalid input, only integers are allowed.")
        continue
    else:
        break

print('\nMaking folders of '+str(numberOfFilesPerFolder) + ' files in '+sourceOfFiles+"\n")

# get all files in directory and sub directories and add into an array of objects
for path, subdirs, files in os.walk(sourceOfFiles):
    files.sort()
    for file in files:
        if file.endswith(".mp4") or file.endswith(".mkv"):
            fileAndFolderList.append({'path': path, 'file': file})

# sort objects in array according to file name
def customSort(k): return k['file']
fileAndFolderList.sort(key=customSort)

# iterate array of objects
for object in fileAndFolderList:
    # create new directory if not existing
    newDirectory = sourceOfFiles + "\\" + str(folderNumber)+" (%s Files)"%numberOfFilesPerFolder
    if(not os.path.exists(newDirectory)):
        directoryExists = False
        os.makedirs(newDirectory)
    else:
        directoryExists = True

    # update folder number based on count of files moved up to not
    fileCounter += 1
    if (fileCounter == numberOfFilesPerFolder):
        folderNumber += 1
        fileCounter = 0

    # carry out file move
    os.replace(object['path']+"\\"+object['file'], newDirectory+"\\"+object['file'])
    print("MOVING:\t\t"+object['path']+"\\"+object['file'] + "\t=>\t"+newDirectory+"\\"+object['file'])
    # print("%sMOVING:\t\t"+object['path']+"\\"+object['name']+"\t=>\t"+newDirectory+"\\"+object['name']+""%("\n==========[USING EXISTING FOLDER]==========\n" if directoryExists else "\n==========[CREATING NEW FOLDER]==========\n"))
print("\nDONE")
input("Press any key to end.")